-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 15, 2017 at 01:47 AM
-- Server version: 5.7.18
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bioskop`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL DEFAULT '-',
  `foto` varchar(100) DEFAULT NULL,
  `jenis_film` varchar(100) NOT NULL DEFAULT '-',
  `produser` varchar(100) NOT NULL,
  `sutradara` varchar(100) NOT NULL DEFAULT '-',
  `penulis` varchar(100) NOT NULL DEFAULT '-',
  `produksi` varchar(100) NOT NULL DEFAULT '-',
  `harga_senin` double NOT NULL DEFAULT '0',
  `harga_selasa` double NOT NULL DEFAULT '0',
  `harga_rabu` double NOT NULL DEFAULT '0',
  `harga_kamis` double NOT NULL DEFAULT '0',
  `harga_jumat` double NOT NULL DEFAULT '0',
  `harga_sabtu` double NOT NULL DEFAULT '0',
  `harga_minggu` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `judul`, `foto`, `jenis_film`, `produser`, `sutradara`, `penulis`, `produksi`, `harga_senin`, `harga_selasa`, `harga_rabu`, `harga_kamis`, `harga_jumat`, `harga_sabtu`, `harga_minggu`) VALUES
(2, 'test 1', 'N2NZRBRLCK.png.png', 'ets', 'test', 'test', 'test', 'test', 10000, 10000, 10000, 10000, 10000, 10000, 10000),
(3, '3333', '0K8QMHSCB6.png.png', '3', '3', '3', '3', '3', 3, 3, 33, 3, 3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `tayang_id` int(11) NOT NULL,
  `kasir_id` int(11) NOT NULL,
  `total` double NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `tayang_id`, `kasir_id`, `total`, `waktu`) VALUES
(1, 3, 1, 125000, '2017-07-15 08:31:54'),
(2, 3, 1, 125000, '2017-07-15 08:38:47');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id` int(11) NOT NULL,
  `pembelian_id` int(11) NOT NULL,
  `harga` double NOT NULL,
  `row` varchar(3) NOT NULL,
  `seat` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id`, `pembelian_id`, `harga`, `row`, `seat`) VALUES
(1, 1, 10000, 'A', 10),
(2, 1, 10000, 'A', 11),
(3, 1, 10000, 'A', 12),
(4, 1, 10000, 'A', 13),
(5, 1, 10000, 'A', 14),
(6, 2, 10000, 'A', 15),
(7, 2, 10000, 'A', 16);

-- --------------------------------------------------------

--
-- Table structure for table `studio`
--

CREATE TABLE `studio` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studio`
--

INSERT INTO `studio` (`id`, `nama`, `type`) VALUES
(2, 'test', 'Test'),
(3, 'test', 'Test'),
(4, 'asdasdasd', 'Besar'),
(5, '', 'Kecil');

-- --------------------------------------------------------

--
-- Table structure for table `tayang`
--

CREATE TABLE `tayang` (
  `id` int(11) NOT NULL,
  `id_film` int(11) NOT NULL,
  `dari_tgl` date NOT NULL,
  `sampai_tgl` date NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `id_studio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tayang`
--

INSERT INTO `tayang` (`id`, `id_film`, `dari_tgl`, `sampai_tgl`, `jam_mulai`, `jam_selesai`, `id_studio`) VALUES
(1, 2, '2017-04-14', '2017-04-14', '17:13:00', '17:13:00', 4),
(2, 3, '2017-07-07', '2017-07-15', '22:00:00', '23:30:00', 4),
(3, 3, '2017-07-07', '2017-07-22', '22:00:00', '23:30:00', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studio`
--
ALTER TABLE `studio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tayang`
--
ALTER TABLE `tayang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `studio`
--
ALTER TABLE `studio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tayang`
--
ALTER TABLE `tayang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

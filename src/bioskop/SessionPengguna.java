/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioskop;

/**
 *
 * @author Irsyad
 */
public class SessionPengguna {
    private Integer id;
    private String nama;
    private String type;
    private Boolean login;

    public SessionPengguna(Integer id, String nama, String type, Boolean login) {
        this.id = id;
        this.nama = nama;
        this.type = type;
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }
    
    
    public Boolean isKasir() {
        if(this.type.equals("Kasir")) {
            return true;
        }
        return false;
    }
    
    public Boolean isPetugas() {
        if(this.type.equals("Petugas")) {
            return true;
        }
        return false;
    }
}

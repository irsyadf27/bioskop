/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbmodel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Irsyad
 */
public class Studio {
    Connection conn;
    ResultSet rs;
    
    public Studio() {
        try {
            conn = (Connection) koneksi.koneksiDB.configDB();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Studio studio) {
        try {
            String sql = "INSERT INTO `studio` (`nama`, `type`) VALUES (?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, studio.getNama());
            pst.setString(2, studio.getType());

            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }

    public Integer update(model.Studio studio) {
        try {
            String sql = "UPDATE `studio` SET `nama` = ?, `type` = ? WHERE `id` = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, studio.getNama());
            pst.setString(2, studio.getType());
            pst.setInt(3, studio.getId());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }
    
    public Integer delete(model.Studio studio) {
        try {
            String sql = "DELETE FROM `studio` WHERE `id` = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, studio.getId());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }
    
    public ArrayList<model.Studio> get() {
        ArrayList<model.Studio> studio = new ArrayList<>();
        try {
            String sql = "SELECT * FROM studio";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                studio.add(
                    new model.Studio(
                        rs.getInt("id"),
                        rs.getString("nama"),
                        rs.getString("type")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data studio");
        }
        return studio;
    }
    
    public ArrayList<model.Studio> search(String keyword) {
        ArrayList<model.Studio> studio = new ArrayList<>();
        try {
            String sql = "SELECT * FROM studio WHERE nama LIKE ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, '%' + keyword + '%');
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                studio.add(
                    new model.Studio(
                        rs.getInt("id"),
                        rs.getString("nama"),
                        rs.getString("type")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data studio");
        }
        return studio;
    }
}

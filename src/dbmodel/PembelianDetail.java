/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbmodel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Irsyad
 */
public class PembelianDetail {
    Connection conn;
    ResultSet rs;
    
    public PembelianDetail() {
        try {
            conn = (Connection) koneksi.koneksiDB.configDB();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.PembelianDetail pembelian) {
        int last_inserted_id = 0;
        try {
            String sql = "INSERT INTO `pembelian_detail` (`pembelian_id`, `harga`, `row`, `seat`) VALUES (?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pst.setInt(1, pembelian.getPembelian_id());
            pst.setDouble(2, pembelian.getHarga());
            pst.setString(3, pembelian.getRow());
            pst.setInt(4, pembelian.getSeat());
            
            pst.executeUpdate();
            
            ResultSet rs = pst.getGeneratedKeys();
            if(rs.next()) {
                last_inserted_id = rs.getInt(1);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return last_inserted_id;
    }
    
    public ArrayList<model.PembelianDetail> getKursi(Integer tayang_id) {
        ArrayList<model.PembelianDetail> pd = new ArrayList<>();
        try {
            String sql = "SELECT pembelian_detail.* FROM pembelian JOIN pembelian_detail ON pembelian_detail.pembelian_id=pembelian.id WHERE pembelian.tayang_id = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, tayang_id);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                pd.add(
                    new model.PembelianDetail(
                        rs.getInt("id"),
                        rs.getInt("pembelian_id"),
                        rs.getDouble("harga"),
                        rs.getString("row"),
                        rs.getInt("seat")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data kursi");
        }
        return pd;
    }
}

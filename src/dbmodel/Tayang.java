/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbmodel;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Irsyad
 */
public class Tayang {
    Connection conn;
    ResultSet rs;
    
    public Tayang() {
        try {
            conn = (Connection) koneksi.koneksiDB.configDB();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Tayang tayang) {
        try {
            String sql = "INSERT INTO `tayang` (`id_film`, `dari_tgl`, `sampai_tgl`, `jam_mulai`, `jam_selesai`, `id_studio`) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, tayang.getId_film());
            pst.setDate(2, tayang.getTgl_mulai());
            pst.setDate(3, tayang.getTgl_selesai());
            pst.setTime(4, tayang.getJam_mulai());
            pst.setTime(5, tayang.getJam_selesai());
            pst.setInt(6, tayang.getId_studio());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }

    public Integer update(model.Tayang tayang) {
        try {
            String sql = "UPDATE `tayang` SET `id_film` = ?, `dari_tgl` = ?, `sampai_tgl` = ?, `jam_mulai` = ?, `jam_selesai` = ?, `id_studio` = ? WHERE `id` = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, tayang.getId_film());
            pst.setDate(2, tayang.getTgl_mulai());
            pst.setDate(3, tayang.getTgl_selesai());
            pst.setTime(4, tayang.getJam_mulai());
            pst.setTime(5, tayang.getJam_selesai());
            pst.setInt(6, tayang.getId_studio());
            pst.setInt(7, tayang.getId());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }
    
    public Integer delete(model.Tayang tayang) {
        try {
            String sql = "DELETE FROM `tayang` WHERE `id` = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, tayang.getId());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }
    
    public ArrayList<model.Tayang> get() {
        ArrayList<model.Tayang> tayang = new ArrayList<>();
        try {
            String sql = "SELECT tayang.*, film.judul, studio.nama AS nama_studio, studio.type AS type_studio FROM tayang "
                    + "JOIN film ON film.id=tayang.id_film "
                    + "JOIN studio ON studio.id=tayang.id_studio ";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                tayang.add(
                    new model.Tayang(
                        rs.getInt("id"),
                        rs.getInt("id_film"),
                        rs.getDate("dari_tgl"),
                        rs.getDate("sampai_tgl"),
                        rs.getTime("jam_mulai"),
                        rs.getTime("jam_selesai"),
                        rs.getInt("id_studio"),
                        rs.getString("judul"),
                        rs.getString("nama_studio"),
                        rs.getString("type_studio")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data tayang");
        }
        return tayang;
    }
    
    public ArrayList<model.Tayang> search(String keyword) {
        ArrayList<model.Tayang> tayang = new ArrayList<>();
        try {
            String sql = "SELECT tayang.*, film.judul, studio.nama AS nama_studio, studio.type AS type_studio FROM tayang "
                    + "JOIN film ON film.id=tayang.id_film "
                    + "JOIN studio ON studio.id=tayang.id_studio "
                    + " WHERE film.judul LIKE ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, '%' + keyword + '%');
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                tayang.add(
                    new model.Tayang(
                        rs.getInt("id"),
                        rs.getInt("id_film"),
                        rs.getDate("dari_tgl"),
                        rs.getDate("sampai_tgl"),
                        rs.getTime("jam_mulai"),
                        rs.getTime("jam_selesai"),
                        rs.getInt("id_studio"),
                        rs.getString("judul"),
                        rs.getString("nama_studio"),
                        rs.getString("type_studio")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data tayang");
        }
        return tayang;
    }
    
    public ArrayList<model.Tayang> cariByTanggal(Date tgl) {
        ArrayList<model.Tayang> tayang = new ArrayList<>();
        try {
            String sql = "SELECT tayang.*, film.judul, studio.nama AS nama_studio, studio.type AS type_studio, film.harga_senin, film.harga_selasa, film.harga_rabu, film.harga_kamis, film.harga_jumat, film.harga_sabtu, film.harga_minggu FROM tayang "
                    + "JOIN film ON film.id=tayang.id_film "
                    + "JOIN studio ON studio.id=tayang.id_studio "
                    + "WHERE tayang.dari_tgl <= ? AND tayang.sampai_tgl >= ? ORDER BY judul, jam_mulai ASC";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setDate(1, tgl);
            pst.setDate(2, tgl);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                tayang.add(
                    new model.Tayang(
                        rs.getInt("id"),
                        rs.getInt("id_film"),
                        rs.getDate("dari_tgl"),
                        rs.getDate("sampai_tgl"),
                        rs.getTime("jam_mulai"),
                        rs.getTime("jam_selesai"),
                        rs.getInt("id_studio"),
                        rs.getString("judul"),
                        rs.getString("nama_studio"),
                        rs.getString("type_studio"),
                        rs.getDouble("harga_senin"),
                        rs.getDouble("harga_selasa"),
                        rs.getDouble("harga_rabu"),
                        rs.getDouble("harga_kamis"),
                        rs.getDouble("harga_jumat"),
                        rs.getDouble("harga_sabtu"),
                        rs.getDouble("harga_minggu")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data tayang");
        }
        return tayang;
    }
    
    public void cboFilm() {
        kelola.Tayang.cboFilm.removeAllItems();
        try {
            String sql = "SELECT * FROM film ORDER BY judul ASC ";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                kelola.Tayang.cboFilm.addItem(new model.CBO(rs.getInt("id"), rs.getString("judul")));	
            }
            
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data film");
        }
    }
    
    public void cboStudio() {
        kelola.Tayang.cboStudio.removeAllItems();
        try {
            String sql = "SELECT * FROM studio ORDER BY nama ASC ";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                kelola.Tayang.cboStudio.addItem(new model.CBO(rs.getInt("id"), rs.getString("nama")));	
            }
            
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data studio");
        }
    }
}

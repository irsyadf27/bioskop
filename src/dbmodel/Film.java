/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbmodel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Irsyad
 */
public class Film {
    Connection conn;
    ResultSet rs;
    
    public Film() {
        try {
            conn = (Connection) koneksi.koneksiDB.configDB();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Film film) {
        try {
            String sql = "INSERT INTO `film` (`judul`, `foto`, `jenis_film`, `produser`, `sutradara`, `penulis`, `produksi`, `harga_senin`, `harga_selasa`, `harga_rabu`, `harga_kamis`, `harga_jumat`, `harga_sabtu`, `harga_minggu`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, film.getJudul());
            pst.setString(2, film.getFoto());
            pst.setString(3, film.getJenis());
            pst.setString(4, film.getProduser());
            pst.setString(5, film.getSutradara());
            pst.setString(6, film.getPenulis());
            pst.setString(7, film.getProduksi());
            
            pst.setDouble(8, film.getHarga_senin());
            pst.setDouble(9, film.getHarga_selasa());
            pst.setDouble(10, film.getHarga_rabu());
            pst.setDouble(11, film.getHarga_kamis());
            pst.setDouble(12, film.getHarga_jumat());
            pst.setDouble(13, film.getHarga_sabtu());
            pst.setDouble(14, film.getHarga_minggu());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }

    public Integer update(model.Film film) {
        try {
            String sql = "UPDATE `film` SET `judul` = ?, `foto` = ?, `jenis_film` = ?, `produser` = ?, `sutradara` = ?, `penulis` = ?, `produksi` = ?, `harga_senin` = ?, `harga_selasa` = ?, `harga_rabu` = ?, `harga_kamis` = ?, `harga_jumat` = ?, `harga_sabtu` = ?, `harga_minggu` = ? WHERE `id` = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, film.getJudul());
            pst.setString(2, film.getFoto());
            pst.setString(3, film.getJenis());
            pst.setString(4, film.getProduser());
            pst.setString(5, film.getSutradara());
            pst.setString(6, film.getPenulis());
            pst.setString(7, film.getProduksi());
            
            pst.setDouble(8, film.getHarga_senin());
            pst.setDouble(9, film.getHarga_selasa());
            pst.setDouble(10, film.getHarga_rabu());
            pst.setDouble(11, film.getHarga_kamis());
            pst.setDouble(12, film.getHarga_jumat());
            pst.setDouble(13, film.getHarga_sabtu());
            pst.setDouble(14, film.getHarga_minggu());
            
            pst.setInt(15, film.getId());
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }
    
    public Integer delete(model.Film film) {
        try {
            String sql = "DELETE FROM `film` WHERE `id` = ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setInt(1, film.getId());
            
            return pst.executeUpdate();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return 0;
    }
    
    public ArrayList<model.Film> get() {
        ArrayList<model.Film> film = new ArrayList<>();
        try {
            String sql = "SELECT * FROM film";
            PreparedStatement pst = conn.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                film.add(
                    new model.Film(
                        rs.getInt("id"),
                        rs.getString("judul"),
                        rs.getString("foto"),
                        rs.getString("jenis_film"),
                        rs.getString("produser"),
                        rs.getString("sutradara"),
                        rs.getString("penulis"),
                        rs.getString("produksi"),
                        rs.getDouble("harga_senin"),
                        rs.getDouble("harga_selasa"),
                        rs.getDouble("harga_rabu"),
                        rs.getDouble("harga_kamis"),
                        rs.getDouble("harga_jumat"),
                        rs.getDouble("harga_sabtu"),
                        rs.getDouble("harga_minggu")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mengambil data film");
        }
        return film;
    }
    
    public ArrayList<model.Film> search(String keyword) {
        ArrayList<model.Film> film = new ArrayList<>();
        try {
            String sql = "SELECT * FROM film WHERE judul LIKE ?";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setString(1, '%' + keyword + '%');
            ResultSet rs = pst.executeQuery();
            
            while (rs.next()) { 
                film.add(
                    new model.Film(
                        rs.getInt("id"),
                        rs.getString("judul"),
                        rs.getString("foto"),
                        rs.getString("jenis_film"),
                        rs.getString("produser"),
                        rs.getString("sutradara"),
                        rs.getString("penulis"),
                        rs.getString("produksi"),
                        rs.getDouble("harga_senin"),
                        rs.getDouble("harga_selasa"),
                        rs.getDouble("harga_rabu"),
                        rs.getDouble("harga_kamis"),
                        rs.getDouble("harga_jumat"),
                        rs.getDouble("harga_sabtu"),
                        rs.getDouble("harga_minggu")
                    )
                );	
            }
            rs.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error mencari data film");
        }
        return film;
    }
}

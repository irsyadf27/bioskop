/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbmodel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Irsyad
 */
public class Pembelian {
    Connection conn;
    ResultSet rs;
    
    public Pembelian() {
        try {
            conn = (Connection) koneksi.koneksiDB.configDB();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Koneksi Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public Integer insert(model.Pembelian pembelian) {
        int last_inserted_id = 0;
        try {
            String sql = "INSERT INTO `pembelian` (`tayang_id`, `kasir_id`, `total`) VALUES (?, ?, ?)";
            PreparedStatement pst = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pst.setInt(1, pembelian.getTayang_id());
            pst.setInt(2, pembelian.getKasir_id());
            pst.setDouble(3, pembelian.getTotal());
            
            pst.executeUpdate();
            
            ResultSet rs = pst.getGeneratedKeys();
            if(rs.next()) {
                last_inserted_id = rs.getInt(1);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return last_inserted_id;
    }
}

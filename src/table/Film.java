/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Irsyad
 */
public class Film extends AbstractTableModel {
    private ArrayList<model.Film> data;

    private final String[] field = {"ID", "Judul", "Jenis Film", "Produksi", "Foto"};

    public void setData(ArrayList<model.Film> data){
            this.data = data;
    }

    @Override
    public int getColumnCount() {
            return field.length;
    }

    @Override
    public int getRowCount() {
            return data.size();
    }

    @Override
    public String getColumnName(int column) {
            return field[column];
    }
    
    public model.Film getDataAt(int baris) {
        return data.get(baris);
    }
    
    @Override
    public Object getValueAt(int baris, int kolom) {
            model.Film p = data.get(baris);
            switch(kolom){
                    case 0:return p.getId();
                    case 1:return p.getJudul();
                    case 2:return p.getJenis();
                    case 3:return p.getProduksi();
                    case 4:return p.getFoto();
                    default:return null;
            }
    }
}

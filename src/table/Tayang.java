/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Irsyad
 */
public class Tayang extends AbstractTableModel {
    private ArrayList<model.Tayang> data;

    private final String[] field = {"ID", "Judul", "Dari Tanggal", "Sampai Tanggal", "Jam Mulai", "Jam Selesai", "Studio"};

    public void setData(ArrayList<model.Tayang> data){
            this.data = data;
    }

    @Override
    public int getColumnCount() {
            return field.length;
    }

    @Override
    public int getRowCount() {
            return data.size();
    }

    @Override
    public String getColumnName(int column) {
            return field[column];
    }
    
    public model.Tayang getDataAt(int baris) {
        return data.get(baris);
    }
    
    @Override
    public Object getValueAt(int baris, int kolom) {
            model.Tayang p = data.get(baris);
            switch(kolom){
                    case 0:return p.getId();
                    case 1:return p.getJudul();
                    case 2:return p.getTgl_mulai();
                    case 3:return p.getTgl_selesai();
                    case 4:return p.getJam_mulai();
                    case 5:return p.getJam_selesai();
                    case 6:return p.getNama_studio();
                    default:return null;
            }
    }
}

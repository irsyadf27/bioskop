/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Irsyad
 */
public class Studio extends AbstractTableModel {
    private ArrayList<model.Studio> data;

    private final String[] field = {"ID", "Nama", "Type"};

    public void setData(ArrayList<model.Studio> data){
            this.data = data;
    }

    @Override
    public int getColumnCount() {
            return field.length;
    }

    @Override
    public int getRowCount() {
            return data.size();
    }

    @Override
    public String getColumnName(int column) {
            return field[column];
    }
    
    @Override
    public Object getValueAt(int baris, int kolom) {
            model.Studio p = data.get(baris);
            switch(kolom){
                    case 0:return p.getId();
                    case 1:return p.getNama();
                    case 2:return p.getType();
                    default:return null;
            }
    }
}

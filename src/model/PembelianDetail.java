/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Time;

/**
 *
 * @author Irsyad
 */
public class PembelianDetail {
    private Integer id;
    private Integer pembelian_id;
    private double harga;
    private String row;
    private Integer seat;

    public PembelianDetail(Integer pembelian_id, double harga, String row, Integer seat) {
        this.pembelian_id = pembelian_id;
        this.harga = harga;
        this.row = row;
        this.seat = seat;
    }

    public PembelianDetail(Integer id, Integer pembelian_id, double harga, String row, Integer seat) {
        this.id = id;
        this.pembelian_id = pembelian_id;
        this.harga = harga;
        this.row = row;
        this.seat = seat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPembelian_id() {
        return pembelian_id;
    }

    public void setPembelian_id(Integer pembelian_id) {
        this.pembelian_id = pembelian_id;
    }

    public double getHarga() {
        return harga;
    }

    public void setHarga(double harga) {
        this.harga = harga;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }
}

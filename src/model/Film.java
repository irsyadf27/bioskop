/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Irsyad
 */
public class Film {
    private Integer id;
    private String judul;
    private String foto;
    private String jenis;
    private String produser;
    private String sutradara;
    private String penulis;
    private String produksi;
    private double harga_senin;
    private double harga_selasa;
    private double harga_rabu;
    private double harga_kamis;
    private double harga_jumat;
    private double harga_sabtu;
    private double harga_minggu;

    public Film(String judul, String foto, String jenis, String produser, String sutradara, String penulis, String produksi, double harga_senin, double harga_selasa, double harga_rabu, double harga_kamis, double harga_jumat, double harga_sabtu, double harga_minggu) {
        this.judul = judul;
        this.foto = foto;
        this.jenis = jenis;
        this.produser = produser;
        this.sutradara = sutradara;
        this.penulis = penulis;
        this.produksi = produksi;
        this.harga_senin = harga_senin;
        this.harga_selasa = harga_selasa;
        this.harga_rabu = harga_rabu;
        this.harga_kamis = harga_kamis;
        this.harga_jumat = harga_jumat;
        this.harga_sabtu = harga_sabtu;
        this.harga_minggu = harga_minggu;
    }

    public Film(Integer id, String judul, String foto, String jenis, String produser, String sutradara, String penulis, String produksi, double harga_senin, double harga_selasa, double harga_rabu, double harga_kamis, double harga_jumat, double harga_sabtu, double harga_minggu) {
        this.id = id;
        this.judul = judul;
        this.foto = foto;
        this.jenis = jenis;
        this.produser = produser;
        this.sutradara = sutradara;
        this.penulis = penulis;
        this.produksi = produksi;
        this.harga_senin = harga_senin;
        this.harga_selasa = harga_selasa;
        this.harga_rabu = harga_rabu;
        this.harga_kamis = harga_kamis;
        this.harga_jumat = harga_jumat;
        this.harga_sabtu = harga_sabtu;
        this.harga_minggu = harga_minggu;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getProduser() {
        return produser;
    }

    public void setProduser(String produser) {
        this.produser = produser;
    }

    public String getSutradara() {
        return sutradara;
    }

    public void setSutradara(String sutradara) {
        this.sutradara = sutradara;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getProduksi() {
        return produksi;
    }

    public void setProduksi(String produksi) {
        this.produksi = produksi;
    }

    public double getHarga_senin() {
        return harga_senin;
    }

    public void setHarga_senin(double harga_senin) {
        this.harga_senin = harga_senin;
    }

    public double getHarga_selasa() {
        return harga_selasa;
    }

    public void setHarga_selasa(double harga_selasa) {
        this.harga_selasa = harga_selasa;
    }

    public double getHarga_rabu() {
        return harga_rabu;
    }

    public void setHarga_rabu(double harga_rabu) {
        this.harga_rabu = harga_rabu;
    }

    public double getHarga_kamis() {
        return harga_kamis;
    }

    public void setHarga_kamis(double harga_kamis) {
        this.harga_kamis = harga_kamis;
    }

    public double getHarga_jumat() {
        return harga_jumat;
    }

    public void setHarga_jumat(double harga_jumat) {
        this.harga_jumat = harga_jumat;
    }

    public double getHarga_sabtu() {
        return harga_sabtu;
    }

    public void setHarga_sabtu(double harga_sabtu) {
        this.harga_sabtu = harga_sabtu;
    }

    public double getHarga_minggu() {
        return harga_minggu;
    }

    public void setHarga_minggu(double harga_minggu) {
        this.harga_minggu = harga_minggu;
    }

    
}

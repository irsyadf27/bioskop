/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Irsyad
 */
public class Pengguna {
    private Integer id;
    private String nama;
    private String password;
    private String type;

    public Pengguna(String nama, String password, String type) {
        this.nama = nama;
        this.password = password;
        this.type = type;
    }

    public Pengguna(Integer id, String nama, String password, String type) {
        this.id = id;
        this.nama = nama;
        this.password = password;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}

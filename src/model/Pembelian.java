/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Irsyad
 */
public class Pembelian {
    private Integer id;
    private Integer tayang_id;
    private Integer kasir_id;
    private double total;
    private Timestamp waktu;

    public Pembelian(Integer tayang_id, Integer kasir_id, double total, Timestamp waktu) {
        this.tayang_id = tayang_id;
        this.kasir_id = kasir_id;
        this.total = total;
        this.waktu = waktu;
    }

    public Pembelian(Integer id, Integer tayang_id, Integer kasir_id, double total, Timestamp waktu) {
        this.id = id;
        this.tayang_id = tayang_id;
        this.kasir_id = kasir_id;
        this.total = total;
        this.waktu = waktu;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTayang_id() {
        return tayang_id;
    }

    public void setTayang_id(Integer tayang_id) {
        this.tayang_id = tayang_id;
    }

    public Integer getKasir_id() {
        return kasir_id;
    }

    public void setKasir_id(Integer kasir_id) {
        this.kasir_id = kasir_id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Timestamp getWaktu() {
        return waktu;
    }

    public void setWaktu(Timestamp waktu) {
        this.waktu = waktu;
    }

}

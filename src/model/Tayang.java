/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author Irsyad
 */
public class Tayang {
    private Integer id;
    private Integer id_film;
    private Date tgl_mulai;
    private Date tgl_selesai;
    private Time jam_mulai;
    private Time jam_selesai;
    private Integer id_studio;
    private String judul;
    private String nama_studio;
    private String type_studio;
    private double harga_senin;
    private double harga_selasa;
    private double harga_rabu;
    private double harga_kamis;
    private double harga_jumat;
    private double harga_sabtu;
    private double harga_minggu;
    
    public Tayang(Integer id_film, Date tgl_mulai, Date tgl_selesai, Time jam_mulai, Time jam_selesai, Integer id_studio) {
        this.id_film = id_film;
        this.tgl_mulai = tgl_mulai;
        this.tgl_selesai = tgl_selesai;
        this.jam_mulai = jam_mulai;
        this.jam_selesai = jam_selesai;
        this.id_studio = id_studio;
    }

    public Tayang(Integer id, Integer id_film, Date tgl_mulai, Date tgl_selesai, Time jam_mulai, Time jam_selesai, Integer id_studio) {
        this.id = id;
        this.id_film = id_film;
        this.tgl_mulai = tgl_mulai;
        this.tgl_selesai = tgl_selesai;
        this.jam_mulai = jam_mulai;
        this.jam_selesai = jam_selesai;
        this.id_studio = id_studio;
    }

    public Tayang(Integer id, Integer id_film, Date tgl_mulai, Date tgl_selesai, Time jam_mulai, Time jam_selesai, Integer id_studio, String judul, String nama_studio, String type_studio) {
        this.id = id;
        this.id_film = id_film;
        this.tgl_mulai = tgl_mulai;
        this.tgl_selesai = tgl_selesai;
        this.jam_mulai = jam_mulai;
        this.jam_selesai = jam_selesai;
        this.id_studio = id_studio;
        this.judul = judul;
        this.nama_studio = nama_studio;
        this.type_studio = type_studio;
    }

    public Tayang(Integer id, Integer id_film, Date tgl_mulai, Date tgl_selesai, Time jam_mulai, Time jam_selesai, Integer id_studio, String judul, String nama_studio, String type_studio, double harga_senin, double harga_selasa, double harga_rabu, double harga_kamis, double harga_jumat, double harga_sabtu, double harga_minggu) {
        this.id = id;
        this.id_film = id_film;
        this.tgl_mulai = tgl_mulai;
        this.tgl_selesai = tgl_selesai;
        this.jam_mulai = jam_mulai;
        this.jam_selesai = jam_selesai;
        this.id_studio = id_studio;
        this.judul = judul;
        this.nama_studio = nama_studio;
        this.type_studio = type_studio;
        this.harga_senin = harga_senin;
        this.harga_selasa = harga_selasa;
        this.harga_rabu = harga_rabu;
        this.harga_kamis = harga_kamis;
        this.harga_jumat = harga_jumat;
        this.harga_sabtu = harga_sabtu;
        this.harga_minggu = harga_minggu;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_film() {
        return id_film;
    }

    public void setId_film(Integer id_film) {
        this.id_film = id_film;
    }

    public Date getTgl_mulai() {
        return tgl_mulai;
    }

    public void setTgl_mulai(Date tgl_mulai) {
        this.tgl_mulai = tgl_mulai;
    }

    public Date getTgl_selesai() {
        return tgl_selesai;
    }

    public void setTgl_selesai(Date tgl_selesai) {
        this.tgl_selesai = tgl_selesai;
    }

    public Time getJam_mulai() {
        return jam_mulai;
    }

    public void setJam_mulai(Time jam_mulai) {
        this.jam_mulai = jam_mulai;
    }

    public Time getJam_selesai() {
        return jam_selesai;
    }

    public void setJam_selesai(Time jam_selesai) {
        this.jam_selesai = jam_selesai;
    }

    public Integer getId_studio() {
        return id_studio;
    }

    public void setId_studio(Integer id_studio) {
        this.id_studio = id_studio;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getNama_studio() {
        return nama_studio;
    }

    public void setNama_studio(String nama_studio) {
        this.nama_studio = nama_studio;
    }

    public String getType_studio() {
        return type_studio;
    }

    public void setType_studio(String type_studio) {
        this.type_studio = type_studio;
    }

    public double getHarga_senin() {
        return harga_senin;
    }

    public void setHarga_senin(double harga_senin) {
        this.harga_senin = harga_senin;
    }

    public double getHarga_selasa() {
        return harga_selasa;
    }

    public void setHarga_selasa(double harga_selasa) {
        this.harga_selasa = harga_selasa;
    }

    public double getHarga_rabu() {
        return harga_rabu;
    }

    public void setHarga_rabu(double harga_rabu) {
        this.harga_rabu = harga_rabu;
    }

    public double getHarga_kamis() {
        return harga_kamis;
    }

    public void setHarga_kamis(double harga_kamis) {
        this.harga_kamis = harga_kamis;
    }

    public double getHarga_jumat() {
        return harga_jumat;
    }

    public void setHarga_jumat(double harga_jumat) {
        this.harga_jumat = harga_jumat;
    }

    public double getHarga_sabtu() {
        return harga_sabtu;
    }

    public void setHarga_sabtu(double harga_sabtu) {
        this.harga_sabtu = harga_sabtu;
    }

    public double getHarga_minggu() {
        return harga_minggu;
    }

    public void setHarga_minggu(double harga_minggu) {
        this.harga_minggu = harga_minggu;
    }

}
